class Producto {
  constructor(
    cantidadProducto,
    productoIngresado,
    costoIngresado,
    descripcionProducto
  ) {
    this.cantidadProducto = parseInt(cantidadProducto);
    this.productoIngresado = productoIngresado.toLowerCase();
    this.costoIngresado = parseFloat(costoIngresado).toFixed(2);
    this.descripcionProducto = descripcionProducto.toLowerCase();
  }
  precioConIva() {
    return (this.costoIngresado * 1.21).toFixed(2);
  }
}

let arrayProducto = JSON.parse(localStorage.getItem("arrayProductos")) || [];

const getAll = () => {
  return arrayProducto;
};

const create = (arrProducto) => {
  arrayProducto.push(arrProducto);
  localStorage.setItem("arrayProductos", JSON.stringify(arrayProducto));
};

console.log(getAll());

const listaProductos = $("#lista-productos");
const formProducto = $("#form-producto");
const inputCantidadProducto = $("#input-cantidad-producto");
const inputNombreProducto = $("#input-nombre-producto");

const inputPrecioProudcto = $("#input-precio-producto");
const inputDescripcionProducto = $("#input-descripcion-producto");

console.log(getAll());

for (let producto of arrayProducto) {
  console.log(producto);
  $("body").append(`Los datos del producto son:
      Nombre del producto: ${producto.productoIngresado} ;
      Cantidad: ${producto.cantidadProducto}; 
      Precio: ${producto.costoIngresado};
      Descripcion: ${producto.descripcionProducto}`);
}
$("#form-producto").submit(function (e) {
  e.preventDefault();
  const cantidadProducto = inputCantidadProducto.value;
  const productoIngresado = inputNombreProducto.value;
  const costoIngresado = inputPrecioProudcto.value;
  const descripcionProducto = inputDescripcionProducto.value;

  const producto = new Producto(
    cantidadProducto,
    productoIngresado,
    costoIngresado,
    descripcionProducto
  );
  create(producto);
});
