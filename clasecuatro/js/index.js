let cantidadProducto = parseInt(
  prompt(
    "======================================= \n Bienvenido, ingrese la cantidad de productos para calcular el IVA: \n ======================================="
  )
);

if (isNaN(cantidadProducto)) {
  alert(
    "*** Por favor recarga la pagina e ingresa los numeros correctamente ***"
  );
} else {
  ingresarCosto(cantidadProducto);
}

function ingresarCosto(cantidadProducto) {
  for (i = 1; i <= cantidadProducto; i++) {
    let costoProducto = parseFloat(
      prompt(`Ingrese el costo del producto ${i} : `)
    );
    if (isNaN(costoProducto)) {
      alert(
        "*** Por favor recarga la pagina e ingresa los numeros correctamente ***"
      );
    } else {
      calcularCosto(costoProducto);
    }
  }
}

function calcularCosto(costoProducto) {
  resultadoIva = costoProducto * 1.21;
  document.write(`<br> El precio final con Iva incluido es ${resultadoIva}`);
}
