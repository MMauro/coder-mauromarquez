let cantidadPersonas = parseInt(
  prompt("Ingrese cantidad de personas para calcular promedio: ")
);
let edad = 0;
let promedio = 0;

if (isNaN(cantidadPersonas)) {
  alert(
    "*** Por favor recarga la pagina e ingresa la cantidad correctamente ***"
  );
} else {
  edadPersonas(cantidadPersonas);
}

function edadPersonas(cantidadPersonas) {
  for (i = 1; i <= cantidadPersonas; i++) {
    edadIngresada = parseInt(prompt("Ingresar edad para calcular promedio: "));
    edad = validarEdad(edadIngresada) + edad;
  }
  calcularPromedio(edad, cantidadPersonas);
}

function validarEdad(edadIngresada) {
  if (isNaN(edadIngresada)) {
    alert(
      "*** Por favor recarga la pagina e ingresa la edad correctamente ***"
    );
  } else {
    return edadIngresada;
  }
}

function calcularPromedio(edad, cantidadPersonas) {
  promedio = edad / cantidadPersonas;
  document.write(`<br/>El promedio de edad es: ${promedio}`);
}
