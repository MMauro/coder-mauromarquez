let numeroSolicitado = parseInt(prompt("Ingresar un Numero: "));

for (let i = 1; i <= 10; i++) {
  let resultado = numeroSolicitado + i;
  alert(numeroSolicitado + " + " + i + " = " + resultado);
}

let nombreIngresado = prompt(
  "Bienvenido alumno, ingrese su nombre y apellido para saber su nota final: "
);

switch (nombreIngresado) {
  case "ALBERTO RODRIGUEZ":
    alert("Su nota es: 6");
    break;
  case "PAOLA RAMIREZ":
    alert("Su nota es: 8");
    break;
  case "LIONEL MESSI":
    alert("Su nota es: 10");
    break;
  case "PABLO GOMEZ":
    alert("Su nota es: 6");
    break;
  case "CECILIA ACOSTA":
    alert("Su nota es: 7");
    break;
  default:
    alert("Ingreso un dato incorrecto");
}
