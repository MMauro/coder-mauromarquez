let cantidadProducto = prompt("Ingrese cantidad de producto: ");

class Producto {
  constructor(
    cantidadProducto,
    productoIngresado,
    costoIngresado,
    descripcionProducto
  ) {
    this.cantidadProducto = parseInt(cantidadProducto);
    this.productoIngresado = productoIngresado.toLowerCase();
    this.costoIngresado = parseFloat(costoIngresado).toFixed(2);
    this.descripcionProducto = descripcionProducto;
  }
  precioConIva() {
    return (this.costoIngresado * 1.21).toFixed(2);
  }
}
const arrProducto1 = [];
for (i = 0; i < cantidadProducto; i++) {
  let productoIngresado = prompt("Ingrese un producto: ");
  let costoIngresado = prompt("Ingrese costo: ");
  let descripcionProducto = prompt("Ingrese descripción de producto: ");

  let productoNuevo = new Producto(
    cantidadProducto,
    productoIngresado,
    costoIngresado,
    descripcionProducto
  );
  arrProducto1.push(productoNuevo);
}
document.write(
  `<h3> Cantidad del Productos adquiridos: ${cantidadProducto}</h3>`
);
for (producto of arrProducto1) {
  document.write(`<h3>Nombre del Producto: ${producto.productoIngresado}</h3>`);
  document.write(
    `<h3>Precio de lista del Producto: ${
      producto.costoIngresado
    }</h3><h3>Precio con IVA: ${producto.precioConIva()}</h3>`
  );
  document.write(
    `<h3>Detalle del Producto: ${producto.descripcionProducto}</h3>`
  );
}

let ordenarCosto = [];
ordenarCosto = arrProducto1.map((elemento) => elemento);
console.log(ordenarCosto);
ordenarCosto = arrProducto1;

ordenarCosto.sort(function (a, b) {
  return a.costoIngresado - b.costoIngresado;
});

document.write(`<h3> Lista de Precios de Productos Ordenados: </h3>`);

for (producto of ordenarCosto) {
  document.write(`<ul><li>Nombre: ${producto.productoIngresado}</ul></li>`);
  document.write(`<ul><li>Detalle: ${producto.descripcionProducto}</ul></li>`);
  document.write(`<ul><li>Costo: ${producto.costoIngresado}</ul></li>`);
}
