productoIngresado = prompt("Ingrese su producto: ");
precioLista = prompt("Ingrese precio de Lista: ");

class Producto {
  constructor(productoIngresado, precioLista) {
    this.productoIngresado = productoIngresado;
    this.precioLista = precioLista;
  }
  precioConIva() {
    return this.precioLista * 1.21;
  }
}

const producto1 = new Producto(productoIngresado, precioLista);
document.write(
  `El producto ${producto1.productoIngresado} tiene precio de lista $${
    producto1.precioLista
  } y precio con IVA de $${producto1.precioConIva()}`
);
