let carrito = [];
let total = 0;
const DOMitems = document.querySelector("#items");
const DOMcarrito = document.querySelector("#carrito");
const DOMtotal = document.querySelector("#total");
const DOMbotonVaciar = document.querySelector("#boton-vaciar");
const miLocalStorage = window.localStorage;

const URL_JSON = "../datos.json";
fetch(URL_JSON)
  .then((response) => response.json())
  .then((datos) => {
    console.log(datos);

    datos.forEach((info) => {
      // Estructura
      const estructura = document.createElement("div");
      estructura.classList.add("col-s4");
      // Body
      const cardBody = document.createElement("div");
      cardBody.classList.add("card");
      // Imagen
      const imagen = document.createElement("img");
      imagen.classList.add("card-image");
      imagen.setAttribute("src", info.imagen);
      // Titulo
      const title = document.createElement("h2");
      title.classList.add("title");
      title.textContent = info.nombre;
      //descripcion
      const description = document.createElement("h5");
      description.classList.add("card-content");
      description.textContent = info.descripcion;
      // Boton
      const boton = document.createElement("a");
      boton.setAttribute("href", "#modal1");
      boton.classList.add(
        "btn",
        "modal-trigger",
        "btn-floating",
        "halfway-fab",
        "waves-effect",
        "waves-light",
        "red"
      );
      const icon = document.createElement("i");
      icon.classList.add("material-icons");

      boton.setAttribute("marcador", info.id);
      boton.addEventListener("click", agregarProducto);
      // Precio
      const precio = document.createElement("h5");
      precio.classList.add("card-content");
      precio.textContent = "Precio: $" + info.precio;

      // Insertamos
      cardBody.appendChild(imagen);
      cardBody.appendChild(title);
      cardBody.appendChild(boton);
      cardBody.appendChild(icon);
      cardBody.appendChild(description);
      cardBody.appendChild(precio);

      estructura.appendChild(cardBody);
      DOMitems.appendChild(estructura);
    });

    function agregarProducto(evento) {
      carrito.push(evento.target.getAttribute("marcador"));
      calcularTotal();
      renderizarCarrito();
      guardarCarritoEnLocalStorage();
    }

    /**
     * Dibuja todos los productos guardados en el carrito
     */

    function renderizarCarrito() {
      DOMcarrito.textContent = "";
      const carritoSinDuplicados = [...new Set(carrito)];
      carritoSinDuplicados.forEach((item) => {
        // Obtenemos el item que necesitamos de la variable base de datos
        const miItem = datos.filter((itemBaseDatos) => {
          return itemBaseDatos.id === parseInt(item);
        });
        // Cuenta el número de veces que se repite el producto
        const numeroUnidadesItem = carrito.reduce((total, itemId) => {
          // Compara Id, Incremento el contador, en caso contrario no mantengo
          return itemId === item ? (total += 1) : total;
        }, 0);

        const estructura = document.createElement("li");
        estructura.classList.add(
          "list-group-item",
          "text-right",
          "caption",
          "center-align"
        );
        estructura.textContent = `Cantidad: ${numeroUnidadesItem} x ${miItem[0].nombre} - Precio Individual: $${miItem[0].precio}`;
        DOMcarrito.appendChild(estructura);

        const miBoton = document.createElement("button");
        miBoton.classList.add("btn", "btn-danger", "mx-5");
        miBoton.textContent = "X";
        miBoton.style.marginLeft = "1rem";
        miBoton.dataset.item = item;
        miBoton.addEventListener("click", borrarItemCarrito);
        estructura.appendChild(miBoton);
      });
    }

    /* * Calcula el precio total teniendo en cuenta los productos repetidos
     */
    function calcularTotal() {
      total = 0;
      carrito.forEach((item) => {
        // De cada elemento obtenemos su precio
        const miItem = datos.filter((itemBaseDatos) => {
          return itemBaseDatos.id === parseInt(item);
        });
        total = total + miItem[0].precio;
      });

      DOMtotal.textContent = total.toFixed(2);
    }

    function guardarCarritoEnLocalStorage() {
      miLocalStorage.setItem("carrito", JSON.stringify(carrito));
    }

    function cargarCarritoDeLocalStorage() {
      if (miLocalStorage.getItem("carrito") !== null) {
        carrito = JSON.parse(miLocalStorage.getItem("carrito"));
      }
    }

    function borrarItemCarrito(evento) {
      // Obtenemos el producto ID que hay en el boton
      const id = evento.target.dataset.item;
      // Borramos todos los productos
      carrito = carrito.filter((carritoId) => {
        return carritoId !== id;
      });
      // volvemos a renderizar
      renderizarCarrito();
      // Calculamos de nuevo el precio
      calcularTotal();
      guardarCarritoEnLocalStorage();
    }

    function vaciarCarrito() {
      // Limpiamos los productos guardados
      carrito = [];
      // Renderizamos los cambios
      renderizarCarrito();
      calcularTotal();
      localStorage.clear();
    }

    DOMbotonVaciar.addEventListener("click", vaciarCarrito);

    cargarCarritoDeLocalStorage();

    calcularTotal();
    renderizarCarrito();
  });
