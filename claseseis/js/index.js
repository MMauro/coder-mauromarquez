let cantidadProducto = prompt("Ingrese cantidad de producto: ");
let productoIngresado = prompt("Ingrese un producto: ");
let costoIngresado = prompt("Ingrese costo: ");
let descripcionProducto = prompt("Ingrese descripción de producto: ");

class Producto {
  constructor(
    cantidadProducto,
    productoIngresado,
    costoIngresado,
    descripcionProducto
  ) {
    this.cantidadProducto = parseInt(cantidadProducto);
    this.productoIngresado = productoIngresado;
    this.costoIngresado = parseFloat(costoIngresado).toFixed(2);
    this.descripcionProducto = descripcionProducto;
  }
  precioConIva() {
    return (this.costoIngresado * 1.21).toFixed(2);
  }
}

const arrProducto1 = [];
let productoNuevo = new Producto(
  cantidadProducto,
  productoIngresado,
  costoIngresado,
  descripcionProducto
);
arrProducto1.push(productoNuevo);

for (producto of arrProducto1) {
  document.write(
    `<h3> Cantidad del producto adquirido: ${producto.cantidadProducto}</h3>`
  );
  document.write(`<h3>Nombre del Producto: ${producto.productoIngresado}</h3>`);
  document.write(
    `<h3>Precio de lista del Producto: ${
      producto.costoIngresado
    }</h3><h3>Precio con IVA: ${producto.precioConIva()}</h3>`
  );
  document.write(
    `<h3>Detalle del Producto: ${producto.descripcionProducto}</h3>`
  );
}
