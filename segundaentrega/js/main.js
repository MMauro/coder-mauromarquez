let carrito = [];
let total = 0;
const DOMitems = document.querySelector("#items");
const DOMcarrito = document.querySelector("#carrito");
const DOMtotal = document.querySelector("#total");
const miLocalStorage = window.localStorage;
// Funciones

/**
 * Se dibuja los productos.
 */
function renderizarProductos() {
  baseDeDatos.forEach((info) => {
    // Estructura
    const estructura = document.createElement("div");
    estructura.classList.add("card", "col-sm-4");
    // Body
    const cardBody = document.createElement("div");
    cardBody.classList.add("card-body");
    // Titulo
    const title = document.createElement("h5");
    title.classList.add("card-title");
    title.textContent = info.nombre;
    // Imagen
    const imagen = document.createElement("img");
    imagen.classList.add("img-fluid");
    imagen.setAttribute("src", info.imagen);
    // Precio
    const precio = document.createElement("p");
    precio.classList.add("card-text");
    precio.textContent = info.precio + "$";
    // Boton
    const boton = document.createElement("button");
    boton.classList.add("btn", "btn-primary");
    boton.textContent = "+";
    boton.setAttribute("marcador", info.id);
    boton.addEventListener("click", agregarProducto);
    // Insertamos
    cardBody.appendChild(imagen);
    cardBody.appendChild(title);
    cardBody.appendChild(precio);
    cardBody.appendChild(boton);
    estructura.appendChild(cardBody);
    DOMitems.appendChild(estructura);
  });
}

/**
 * Añadir producto
 */
function agregarProducto(evento) {
  carrito.push(evento.target.getAttribute("marcador"));
  calcularTotal();
  renderizarCarrito();
  guardarCarritoEnLocalStorage();
}

/**
 * Dibuja todos los productos guardados en el carrito
 */
function renderizarCarrito() {
  DOMcarrito.textContent = "";
  const carritoSinDuplicados = [...new Set(carrito)];
  carritoSinDuplicados.forEach((item) => {
    // Obtenemos el item que necesitamos de la variable base de datos
    const miItem = baseDeDatos.filter((itemBaseDatos) => {
      return itemBaseDatos.id === parseInt(item);
    });
    // Cuenta el número de veces que se repite el producto
    const numeroUnidadesItem = carrito.reduce((total, itemId) => {
      // Compara Id, Incremento el contador, en caso contrario no mantengo
      return itemId === item ? (total += 1) : total;
    }, 0);

    const estructura = document.createElement("li");
    estructura.classList.add("list-group-item", "text-right", "mx-2");
    estructura.textContent = `${numeroUnidadesItem} x ${miItem[0].nombre} - ${miItem[0].precio}$`;
    DOMcarrito.appendChild(estructura);
  });
}

/**
 * Calcula el precio total teniendo en cuenta los productos repetidos
 */
function calcularTotal() {
  total = 0;
  carrito.forEach((item) => {
    // De cada elemento obtenemos su precio
    const miItem = baseDeDatos.filter((itemBaseDatos) => {
      return itemBaseDatos.id === parseInt(item);
    });
    total = total + miItem[0].precio;
  });

  DOMtotal.textContent = total.toFixed(2);
}

function guardarCarritoEnLocalStorage() {
  miLocalStorage.setItem("carrito", JSON.stringify(carrito));
}

function cargarCarritoDeLocalStorage() {
  if (miLocalStorage.getItem("carrito") !== null) {
    carrito = JSON.parse(miLocalStorage.getItem("carrito"));
  }
}

// Inicio

cargarCarritoDeLocalStorage();
renderizarProductos();
calcularTotal();
renderizarCarrito();
